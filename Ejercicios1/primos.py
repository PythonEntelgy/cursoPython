n = int(input('Introduzca el nº '))


for i in range(1, n + 1):
    tieneDivisores = False
    for j in range(2, i):
        if i % j == 0:
            tieneDivisores = True
            break
    if not tieneDivisores:
        print(i, 'es primo')
